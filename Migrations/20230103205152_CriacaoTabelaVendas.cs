﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace techtestpaymentapi.Migrations
{
    /// <inheritdoc />
    public partial class CriacaoTabelaVendas : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vendas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    cpfVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    nomeVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    emailVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    telefoneVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dataDaVenda = table.Column<DateTime>(type: "datetime2", nullable: false),
                    idPedido = table.Column<int>(type: "int", nullable: false),
                    itens = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendas", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vendas");
        }
    }
}
