using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;


namespace TrilhaApiDesafio.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class VendaController : ControllerBase
    {
        private readonly SistemaVendasContext _context;

        public VendaController(SistemaVendasContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistarVenda(Venda venda)
        {
            if(venda.itens == null)
            {
                return BadRequest(new { Erro = "Uma venda não pode ocorrer sem itens." });
            }
            if (venda.dataDaVenda == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia." });
            if(venda.status != EnumStatusVenda.AguardandoPagamento)
            {
                return BadRequest(new { Erro = "A venda não pode ser registrada com status diferente de AguardandoPagamento." });
            }

            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.id }, venda);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, string status)
        {
            string [] stringStatusVenda = {"AguardandoPagamento", 
               "PagamentoAprovado", "EnviadoParaTransportadora",
                "Entregue", "Cancelada",};

            var vendaBanco = _context.Vendas.Find(id);
            if (vendaBanco == null)
            {
                return NotFound();
            }
            if (vendaBanco.status.ToString() == stringStatusVenda[0] && 
                status == stringStatusVenda[1]) //Aguardando pagamento -> Pagamento aprovado
            {   
                vendaBanco.status = EnumStatusVenda.PagamentoAprovado;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);        
            }
            else if (vendaBanco.status.ToString() == stringStatusVenda[0] && 
                status == stringStatusVenda[4]) //Aguardando pagamento -> Cancelada
            {   
                vendaBanco.status = EnumStatusVenda.Cancelada;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);        
            }

            else if (vendaBanco.status.ToString() == stringStatusVenda[1] && 
                status == stringStatusVenda[2]) //Pagamento aprovado -> Enviado para Transportadora
            {
                vendaBanco.status = EnumStatusVenda.EnviadoParaTransportadora;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);        
            }

            else if (vendaBanco.status.ToString() == stringStatusVenda[1] && 
                status == stringStatusVenda[4]) //Pagamento aprovado -> Cancelada
            {   
                vendaBanco.status = EnumStatusVenda.Cancelada;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);        
            }

            else if (vendaBanco.status.ToString() == stringStatusVenda[2] && 
                status == stringStatusVenda[3]) //Enviado a transportadora -> Entregue
            {
                vendaBanco.status = EnumStatusVenda.Entregue;
                _context.Vendas.Update(vendaBanco);
                _context.SaveChanges();
                return Ok(vendaBanco);        
            }
            else
                return BadRequest(new { Erro = "Operação Inválida!" });            
        }
    }
}