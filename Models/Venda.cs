using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int id { get; set; }
        public int idVendedor { get; set; }
        public string cpfVendedor  { get; set; }
        public string nomeVendedor { get; set; }
        public string emailVendedor { get; set; }
        public string telefoneVendedor { get; set; }
        public DateTime dataDaVenda { get; set; }
        public int idPedido { get; set; }
        public string itens { get; set; } 
        public EnumStatusVenda status { get; set; }

    }
}